package com.example.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.util.IOUtils;


 
@Controller
public class UploadController {
 
	private Logger logger = LoggerFactory.getLogger(UploadController.class);
	
	
	//get data from property file
	
	@Value("${jsa.aws.access_key_id}")
	private String awsId;
	
	@Value("${jsa.aws.secret_access_key}")
	private String awsKey;
	
	@Value("${jsa.s3.region}")
	private String region;
	
	@Value("${jsa.s3.bucket}")
	private String bucketName;

	private byte[] contentBytes;
	
	
	//load the web page in the initial stage
	
	@GetMapping("/")
	public String listUploadedFiles(Model model) {
		return "uploadForm";
	}
 
	
	//when click on the submit button this method will fire
	
	@PostMapping("/")
	public String handleFileUpload(@RequestParam("file") MultipartFile file, Model model) {
		try {
						
			System.out.println("---------------- START UPLOAD FILE ----------------");
			this.uploadFile(file);
			
			
			//print message in the web page
			model.addAttribute("message", "You have successfully uploaded " + file.getOriginalFilename() + "!");
			
			
		} catch (Exception e) {
			model.addAttribute("message", "Fail to upload " + file.getOriginalFilename() + "!");
		}
		
		return "uploadForm";
	}
	

	
	
 
	public void uploadFile(MultipartFile file) throws IllegalStateException, IOException {
		
		
		//create authentication credentials
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsId, awsKey);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
								.withRegion(Regions.fromName(region))
		                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
		                        .build();
		
		
		try {
			//get input stream in order to create metadata
			InputStream inpStrm = file.getInputStream();
			contentBytes = IOUtils.toByteArray(inpStrm);
			
		} catch (IOException e) {
		    System.err.printf("Failed while reading bytes from %s", e.getMessage());
		} 
		
		
		Long contentLength = Long.valueOf(contentBytes.length);
		
		//create the metadata object
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(contentLength);
		
		//get the input stream data for pass to the S3 object
		InputStream inputStream = file.getInputStream();
		
		try {
			
			//connect with S3 objects
	       s3Client.putObject(new PutObjectRequest(bucketName, file.getOriginalFilename(),inputStream, metadata));
	       logger.info("===================== Upload File - Done! =====================");
	        
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException from PUT requests, rejected reasons:");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            logger.info("Caught an AmazonClientException: ");
            logger.info("Error Message: " + ace.getMessage());
        }finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
	}
	
}
